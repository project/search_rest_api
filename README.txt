Introduction
-------------
Extention of core search module that provides scope to integrate with any 
3rd party platform in terms of showing contents after side wide searching for a
given keyword. The communication is based on conventional RESTful API to make it
easier to integrate. 

Dependencies
-------------
Needs drupal core's search module to be enabled.


Installation & Configuration
-----------------------------
Regular module installation process. There no modules settings page yet.

Author
------
Sheikh Faiyaz Moorsalin
sheikh303@gmail.com
